<?php
/*
 |--------------------------------------------------------------------------
 | dbkl dynamic value - 20171109(Ain)
 |--------------------------------------------------------------------------
*/
return [   
    /* general */
    'currency' => 'MYR',
    'currency_code' => 'RM',    
    'dropdown_default' => '----- Sila Pilih -----', // default dropdown
    'print_div' => 'printableArea', // class for print function
    'invoice' => 'Bil', // name replacement for invoice
    'tax_invoice' => 'Bil Cukai', // name replacement for tax invoice

    /* Modul */
    'baca_meter' => 'Baca Meter',
    'modul' => 'Modul',
    /*
    |--------------------------------------------------------------------------
    | Assets paths
    |--------------------------------------------------------------------------
    |
    | Location of all application assets, relative to the public folder,
    | may be used together with absolute paths or with URLs.
    |
    */

    'images' => '/horizontal/assets/images',
    'css' => '/horizontal/assets/css',
    'img' => '/assets/img',
    'js' => '/horizontal/assets/js',

    'cajPembangunan' => 'caj-pembangunan/',
    'bilAir' => 'bil-air/',
    'terimaanPelbagai' => 'terimaan-pelbagai/',
    'taksiran' => 'taksiran/',
    'kutipan' => 'kutipan/',
    
    /* button */
    'create' => 'Daftar',
    'create_btn' => 'btn btn-90 btn-info',
    
    'update' => 'Kemaskini',
    'update_btn' => 'btn btn-90 btn-info',

    'save' => 'Simpan',
    'save_btn' => 'btn btn-90 btn-info',

    'view_profil' => 'Profil',
    'view_profil_btn' => 'btn btn-90 btn-info',
    'view_profil_icon' => 'fa fa-eye',
    
    'cancel' => 'Batal',
    'cancel_btn' => 'btn btn-90 btn-secondary',

    'close' => 'Tutup',
    
    'register' => 'Daftar ',
    'apply' => 'Mohon',
    'register_btn' => 'btn btn-success',
    'register_icon' => 'fa fa-plus-circle p-r-3',

   
    
    'search' => 'Carian',
    'search_btn' => 'btn btn-90 btn-primary',
    'search_icon' => 'fa fa-search p-r-3',
    
    'reset' => 'Reset',
    'reset_btn' => 'btn btn-90 btn-inverse',
    'reset_icon' => 'fa fa-refresh p-r-3',  
    
    'export' => 'Eksport',
    'export_btn' => 'btn btn-info btn-90 dropdown-toggle',
    'export_icon' => 'mdi mdi-file-export p-r-3',  
    
    'add' => 'Tambah',
    'add_btn' => 'btn btn-success',
    'add_icon' => 'fa fa-plus-circle',

    'addnew' => 'Mohon',
    'addnew_btn' => 'btn btn-90 btn-success',
    'addnew_icon' => 'fa fa-plus-circle p-r-3',

    'upload' => 'Memuatnaik',
    'upload_btn' => 'btn btn-90 btn-info',
    'upload_icon' => 'fa mdi-folder-upload p-r-3',

    'pay' => 'Buat Pembayaran',
    'pay_btn' => 'btn btn-success',

    'back' => 'Kembali',
    'back_btn' => 'btn btn-info',
    'back_icon' => 'fa fa-arrow-left',
    
    /* action button */
    'edit_btn_action' => 'btn btn-info',
    'edit_icon_action' => 'fa fa-pencil',  
    
    'delete_btn_action' => 'btn btn-danger',
    'delete_icon_action' => 'fa fa-trash-o',  
    
    'view_btn_action' => 'btn btn-secondary',
    'view_icon_action' => 'fa fa-eye',
    
    'next' => 'Seterusnya',
    'next_btn' => 'btn btn-90 btn-secondary',
    'previous' => 'Sebelumnya',
    'previous_btn' => 'btn btn-90 btn-secondary',

     //suliana
     'report' => 'Laporan',
     'report_btn' => 'btn btn-90 btn-info',
 
     'process' => 'Proses',
     'process_btn' => 'btn btn-90 btn-info',

     'daftar_pelanggan' => 'Daftar Pelanggan',
     'daftar_pelanggan_btn' => 'btn btn-success',
     'daftar_pelanggan_icon' => 'fa fa-plus-circle p-r-3',
 
     'daftar_sewa' => 'Daftar Sewa',
     'daftar_sewa_btn' => 'btn btn-success',
     'daftar_sewa_icon' => 'fa fa-plus-circle p-r-3',
     

    // 20171115 (luqman)
    'print' => 'Cetak',
    'print_btn_action' => 'btn btn-secondary',
    'print_icon_action' => 'fa fa-print',

    'send' => 'Hantar',
    'send_btn_action' => 'btn btn-90 btn-info',
    //'send_icon_action' => 'fa fa-paper-plane',
    
    'save' => 'Simpan',
    'save_btn' => 'btn btn-90 btn-info',

    'detail' => 'Terperinci',
    'detail_btn_action' => 'btn btn-primary',
    'detail_icon_action' => 'fa fa-eye',  
    
    'cancel' => 'Batal',
    'cancel_btn' => 'btn btn-warning',
    'cancel_icon' => 'fa fa-window-close',

    'block' => 'Sekat',
    'block_btn' => 'btn btn-danger',
    'block_icon' => 'mdi mdi-block-helper',

    'caj_lewat' => 'Caj Lewat',
    'lewat_btn' => 'btn btn-90 btn-info',
    'lewat_icon' => 'fa mdi-folder-upload p-r-3',
];