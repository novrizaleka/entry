<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/rijal', function () {
    //return view('welcome');
    return view('rijal');
});

Route::get('/', function () {
    return view('auth/login');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
Route::get('/entry/{account}', 'DataEntry\EntryController@entry')->name('entry');
Route::get('/register_entry', 'DataEntry\RegisterEntryController@index')->name('register_entry');
Route::get('/agent_type/{id}', 'ApiController@agent_type');
Route::get('/card_branch_state/{id}', 'ApiController@card_branch_state');
Route::get('/product_type/{id}', 'ApiController@product_type');


//REGISTER FEP
Route::group(['prefix'=>'register'], function() {
    Route::get('/', 'Register\RegisterController@index')->name("register");
    Route::get('/image/{id}', 'Register\RegisterController@image_view');
	Route::post('/image', 'Register\RegisterController@done_register_image');
});


//ISSUE TO ENTRY
Route::group(['prefix'=>'issue_to_entry'], function() {
    Route::get('/', 'Register\IssueToDataEntryController@index')->name('issue_to_entry');
	Route::post('/save', 'Register\IssueToDataEntryController@save_issue_entry');
});


//ISSUE TO VERIFY
Route::group(['prefix'=>'issue_verify'], function() {
});

//DATA ENTRY
Route::group(['prefix'=>'data_entry'], function() {
   	Route::get('/', 'DataEntry\DataEntryController@index')->name('data_entry');
   	Route::post('/post_entry', 'DataEntry\EntryController@post_entry');
	/*Route::resource('/step_2', 'DataEntry\Step2Controller');
	Route::resource('/step_3', 'DataEntry\Step3Controller');
	Route::resource('/step_4', 'DataEntry\Step4Controller');
	Route::resource('/step_5', 'DataEntry\Step5Controller');*/
	Route::get('/step_1/{account}', 'DataEntry\Step1Controller@step1');
	Route::get('/step_2/{account}', 'DataEntry\Step2Controller@step2');
	Route::get('/step_3/{account}', 'DataEntry\Step3Controller@step3');
	Route::get('/step_4/{account}', 'DataEntry\Step4Controller@step4');
	Route::get('/step_5/{account}', 'DataEntry\Step5Controller@step5');
	Route::get('/step_6/{account}', 'DataEntry\Step6Controller@step6');
	Route::get('/step_7/{account}', 'DataEntry\Step7Controller@step7');
	Route::get('/step_8/{account}', 'DataEntry\Step8Controller@step8');
	Route::get('/step_9/{account}', 'DataEntry\Step9Controller@step9');


	Route::post('/save_step1', 'DataEntry\Step1Controller@save_step1');
	Route::post('/save_step2', 'DataEntry\Step2Controller@save_step2');
	Route::post('/save_step3', 'DataEntry\Step3Controller@save_step3');
	Route::post('/save_step4', 'DataEntry\Step4Controller@save_step4');
	Route::post('/save_step5', 'DataEntry\Step5Controller@save_step5');
	Route::post('/save_step6', 'DataEntry\Step6Controller@save_step6');
	Route::post('/save_step7', 'DataEntry\Step7Controller@save_step7');
	Route::post('/save_step8', 'DataEntry\Step8Controller@save_step8');
	Route::post('/save_step9', 'DataEntry\Step9Controller@save_step9');
});

Route::resource('verifiedForm', 'DataEntry\TempEntryFormController'); 
 Route::get('/data_entry/form_entry/{account}', 'DataEntry\EntryController@entry');


Route::group(['prefix'=>'register_data'], function() {
    Route::get('/', 'Register\RegisterController@index')->name("register_data");
});

//Route::get('image', 'Register\RegisterController@image');




Route::get('/autocomplete', $arrayName = array('as' => 'autocomplete', 'uses'=>'Register\IssueToDataEntryController@autocomplete'));


Route::group(['prefix'=>'api'], function() {
   	Route::get('/sc_channel', 'ApiController@sc_channel');
	Route::get('/sc_branch', 'ApiController@sc_branch');
	Route::get('/agenttype', 'ApiController@agenttype');
	Route::get('/agentnumber', 'ApiController@agentnumber');
	Route::get('/carddelivery', 'ApiController@carddelivery');
	Route::get('/cardcollection', 'ApiController@cardcollection');
	Route::get('/cardbranchstate', 'ApiController@cardbranchstate');
	Route::get('/collectbranch', 'ApiController@collectbranch');
	Route::get('/plasticdata', 'ApiController@plasticdata');
	Route::get('/businessowner', 'ApiController@businessowner');

	//step 4
	Route::get('/idtype', 'ApiCIFController@idtype');
	Route::get('/idtypealt', 'ApiCIFController@idtypealt');
	Route::get('/salutation', 'ApiCIFController@salutation');
	Route::get('/sex', 'ApiCIFController@sex');
	Route::get('/race', 'ApiCIFController@race');
	Route::get('/religion', 'ApiCIFController@religion');
	Route::get('/ethnic', 'ApiCIFController@ethnic');
	Route::get('/nationality', 'ApiCIFController@nationality');
	Route::get('/citizenship', 'ApiCIFController@citizenship');
	Route::get('/marital', 'ApiCIFController@marital');
	Route::get('/education', 'ApiCIFController@education');
	Route::get('/ambank_staff', 'ApiCIFController@ambank_staff');
	Route::get('/vip', 'ApiCIFController@vip');
	Route::get('/title', 'ApiCIFController@title');
	Route::get('/emp_type', 'ApiCIFController@emp_type');
	//step5
	Route::get('/mailing', 'ApiCIFController@mailing');
	Route::get('/residence', 'ApiCIFController@residence');
	//step8
	Route::get('/nature', 'ApiCIFController@nature');
	Route::get('/occupation', 'ApiCIFController@occupation');
	Route::get('/job_sector', 'ApiCIFController@job_sector');
	Route::get('/designation', 'ApiCIFController@designation');
	//step 9
	Route::get('/relationship', 'ApiCIFController@relationship');
});

Route::get('/postcode/{id}', 'ApiController@postcode');