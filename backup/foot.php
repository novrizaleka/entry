@section('footer')
    <script src="{{ asset('assets/public/assets/lib/jquery/jquery.js') }}"></script>

    <script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.14.1/moment.min.js"></script>
    <script src="//cdnjs.cloudflare.com/ajax/libs/Uniform.js/2.1.2/jquery.uniform.min.js"></script>
    <script src="//cdnjs.cloudflare.com/ajax/libs/chosen/1.1.0/chosen.jquery.min.js"></script>
    <script src="//cdnjs.cloudflare.com/ajax/libs/jquery-tagsinput/1.3.3/jquery.tagsinput.min.js"></script>
    <script src="//cdnjs.cloudflare.com/ajax/libs/autosize.js/1.18.17/jquery.autosize.min.js"></script>
    <script src="//cdnjs.cloudflare.com/ajax/libs/jasny-bootstrap/3.1.3/js/jasny-bootstrap.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-switch/3.3.2/js/bootstrap-switch.min.js"></script>
    <script src="//cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.3.1/js/bootstrap-datepicker.min.js"></script>
    <script src="//cdnjs.cloudflare.com/ajax/libs/bootstrap-colorpicker/2.0.1/js/bootstrap-colorpicker.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datetimepicker/4.17.37/js/bootstrap-datetimepicker.min.js"></script>


    <!--Bootstrap -->
    <script src=" {{ asset('assets/public/assets/lib/bootstrap/js/bootstrap.js') }}"></script>
    <!-- MetisMenu -->
    <script src="{{ asset('assets/public/assets/lib/metismenu/metisMenu.js') }}"></script>
    <!-- onoffcanvas -->
    <script src="{{ asset('assets/public/assets/lib/onoffcanvas/onoffcanvas.js') }}"></script>
    <!-- Screenfull -->
    <script src="{{ asset('assets/public/assets/lib/screenfull/screenfull.js') }}"></script>

    <script src=" {{ asset('assets/public/assets/lib/inputlimiter/jquery.inputlimiter.js') }}"></script>
    <script src=" {{ asset('assets/public/assets/lib/validVal/js/jquery.validVal.min.js') }}"></script>
    <script src=" {{ asset('assets/public/assets/lib/bootstrap-daterangepicker/daterangepicker.js') }}"></script>

    <script src="{{ asset('assets/public/assets/js/core.js') }}"></script>
    <!-- Metis demo scripts -->
    <script src="{{ asset('assets/public/assets/js/app.js') }}"></script>
    
 <script src="{{ asset('assets/lib/formwizard/js/jquery.form.wizard.js') }}"></script>
    <script>
                    $(function() {
                      Metis.formWizard();
                    });
                </script>

    <script src="{{ asset('assets/public/assets/js/style-switcher.js') }}"></script>
@stop