@extends('public.master4')

@section('content')

<style type="text/css">
    #content, html, body {
    height: 98%;
}
#left {
    float: left;
    width: 50%;
    background: red;
    height: 1000px;
    overflow: scroll;
}
#right {
    float: left;
    width: 50%;
    background: blue;
    height: 1000px;
    overflow: scroll;
}
</style>

<div id="content">
           <div class="row">
                <div class="col-md-6" style="overflow: scroll;">
                    <div class="box dark">
                        <!--<header>
                            <div class="icons"><i class="glyphicon glyphicon-edit"></i></i></div>
                                <h5><input type="text" name="account" value="{{$temp_entry->account}}"></h5>

                                <div class="toolbar">
                                  <nav style="padding: 8px;">
                                    <a href="javascript:;" class="btn btn-default btn-xs collapse-box">
                                        <i class="glyphicon glyphicon-minus"></i>
                                    </a>
                                    <a href="#borderedTable" data-toggle="collapse" class="btn btn-default btn-sm minimize-box">
                                        <i class="glyphicon glyphicon-chevron-up"></i>
                                    </a>
                                    <a class="btn btn-danger btn-sm close-box">
                                        <i class="glyphicon glyphicon-remove"></i>
                                    </a>
                                  </nav>
                                </div>
                        </header>-->
                        <div id="div-1" class="body" style="background-color: pink">

                            <iframe src="{{url('/')}}/data_entry/step_4/{{$temp_entry->account}}" border="0" framspacing="0" marginheight="0" marginwidth="0" vspace="0" hspace="0" frameborder="0"  height="680px"  width="100%"></iframe>
                        </div>
                    </div>
                </div>
                
                <div class="col-md-6" style="overflow: scroll; padding: 0px !important">
                     <div class="box dark">
                        <!--<header>
                            <div class="icons"><i class="glyphicon glyphicon-edit"></i></i></div>
                                <h5><input type="text" name="account" value="{{$temp_entry->account}}"></h5>

                                <div class="toolbar">
                                  <nav style="padding: 8px;">
                                    <a href="javascript:;" class="btn btn-default btn-xs collapse-box">
                                        <i class="glyphicon glyphicon-minus"></i>
                                    </a>
                                    <a href="#borderedTable" data-toggle="collapse" class="btn btn-default btn-sm minimize-box">
                                        <i class="glyphicon glyphicon-chevron-up"></i>
                                    </a>
                                    <a class="btn btn-danger btn-sm close-box">
                                        <i class="glyphicon glyphicon-remove"></i>
                                    </a>
                                  </nav>
                                </div>
                        </header>-->
                         <iframe src="{{url('/')}}/documents/{{$temp_entry->account}}.pdf"  border="0" framspacing="0" marginheight="0" marginwidth="0" vspace="0" hspace="0" frameborder="0" height="680px"  width="100%">

                            <object height="720px"  width="100%" type="application/pdf" data="http://localhost:8080/icopangkasa2/public/documents/CCA-2018-0062396_850418016087.pdf" id="pdf_content">
                                <p>Insert your error message here, if the PDF cannot be displayed.</p>
                            </object></iframe>
                    </div>
                </div>
            </div>
    </div>
@endsection

