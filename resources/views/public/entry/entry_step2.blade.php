@extends('public.master4')

@section('content')

<style type="text/css">
    #content, html, body {
    height: 98%;
}
#left {
    float: left;
    width: 50%;
    background: red;
    height: 1000px;
    overflow: scroll;
}
#right {
    float: left;
    width: 50%;
    background: blue;
    height: 1000px;
    overflow: scroll;
}
</style>

<div id="content">
    <div class="outer">
        <div class="inner bg-light lter">
           <div class="row">
                <div class="col-lg-6" style="overflow: scroll;">
                    <div class="box dark">
                        <header>
                            <div class="icons"><i class="fa fa-edit"></i></div>
                                <h5>Input Text Fields</h5>
                                <!-- .toolbar -->
                                <div class="toolbar">
                                  <nav style="padding: 8px;">
                                      <a href="javascript:;" class="btn btn-default btn-xs collapse-box">
                                          <i class="fa fa-minus"></i>
                                      </a>
                                      <a href="javascript:;" class="btn btn-default btn-xs full-box">
                                          <i class="fa fa-expand"></i>
                                      </a>
                                      <a href="javascript:;" class="btn btn-danger btn-xs close-box">
                                          <i class="fa fa-times"></i>
                                      </a>
                                  </nav>
                                </div><!-- /.toolbar -->
                        </header>
                        <div id="div-1" class="body">
                            <form class="s-submit clearfix" enctype="multipart/form-data" method="POST" action="{{url('/data_entry/step_2')}}">
                            {{csrf_field()}}

                               <fieldset class="step" id="second">
                                <h4 class="text-danger pull-right">Choice of Card</h4>
                                <div class="clearfix"></div>
                                <div class="form-group">
                                    <label for="parameter" class="control-label col-lg-4">Product Type<sup>*</sup></label>

                                    <div class="col-lg-6">
                                         <select name="product_type" id="product_type" class="form-control" required="">
                                            <option selected="">PLEASE SELECT</option>
                                                @foreach ($product_type as $product_type)
                                                    <option value="{{$product_type->programme_code }}">{{ $product_type->programme_code }}</option>
                                                @endforeach
                                        </select>
                                        <input type="text" name="account" id="account" value="{{$temp_entry->account}}">
                                    </div>
                                </div>
                                   <table class="table table-bordered orang-dihubungi"> 
                                        <thead>
                                            <th width="5%"></th>
                                            <th width="15%"><b>Prg Code</b></th>
                                            <th width="15%"><b>Card TyPe</b></th>
                                            <th width="15%"><b>Gift</b></th>
                                            <th width="5%"> <b>Zing Ind (Y/N)</b></th>
                                        </thead>
                                        
                                         <tr>
                                            <td>1</td>
                                            <td>
                                                <select name="program_code[]" id="program_code" class="form-control">
                                                    <option selected="">PLEASE SELECT</option>
                                                       
                                                </select>
                                            </td>
                                            <td>
                                               <input type="text" name="card_type" id="card_type"></td>
                                            <td>
                                                <input type="text" name="gift_code" id="gift_code"></td>
                                            <td>
                                                <input type="text" name="zing_ind" id="zing_ind"></td>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>2</td>
                                            <td>
                                                <select name="program_code[]" id="program_code" class="form-control">
                                                    <option selected="">PLEASE SELECT</option>
                                                       
                                                </select>
                                            </td>
                                            <td>
                                                <select name="card_type[]" id="card_type" class="form-control chzn-select">
                                                    <option selected="">PLEASE SELECT</option>
                                                        @foreach ($card_type2 as $card_type2)
                                                            <option value="{{$card_type2->id }}">{{ $card_type2->card_type_desc }}</option>
                                                        @endforeach
                                                </select></td>
                                            <td>
                                                <select name="gift_code[]" id="gift_code" class="form-control chzn-select">
                                                    <option selected="">PLEASE SELECT</option>
                                                        @foreach ($gift_code2 as $gift_code3)
                                                            <option value="{{$gift_code3->id }}">{{ $gift_code3->gift_code_desc }}</option>
                                                        @endforeach
                                                </select></td>
                                            <td>
                                                <select name="zing_ind" id="zing_ind" class="form-control" required="">
                                                    <option selected="">PLEASE SELECT</option>
                                                    <option value="Y">Y</option>
                                                    <option value="N">N</option>
                                                </select></td>
                                        </tr>
                                    </table>
                                    <div class="form-group">
                                    <label for="parameter" class="control-label col-lg-4">Promotion Program<sup>*</sup></label>
                                    <div class="col-lg-6">
                                         <select name="promotion_program" id="promotion_program" class="form-control" required="">
                                            <option selected="">PLEASE SELECT</option>
                                                @foreach ($promotion_program as $promotion_program)
                                                    <option value="{{$promotion_program->promotion_code }}">{{ $promotion_program->promotion_desc }}</option>
                                                @endforeach
                                        </select>
                                    </div>
                                </div>
                                
                               
                            
                            </fieldset>
                                <a href="{{url('/data_entry/step_1')}}"><button type="button" class="btn m-btn pull-left">Prev</button></a>
                                <button type="submit" class="btn m-btn pull-right">Next</button>
                            </form>
                        </div>
                    </div>
                </div>
                
                <!--<div class="col-lg-6" style="overflow: scroll;">
                     <div class="box dark">
                        <header>
                            <div class="icons"><i class="fa fa-edit"></i></div>
                                <h5>Input Text Fields</h5>
                                <div class="toolbar">
                                  <nav style="padding: 8px;">
                                      <a href="javascript:;" class="btn btn-default btn-xs collapse-box">
                                          <i class="fa fa-minus"></i>
                                      </a>
                                      <a href="javascript:;" class="btn btn-default btn-xs full-box">
                                          <i class="fa fa-expand"></i>
                                      </a>
                                      <a href="javascript:;" class="btn btn-danger btn-xs close-box">
                                          <i class="fa fa-close"></i>
                                      </a>
                                  </nav>
                                </div>
                        </header>
                            <object height="720px"  width="100%" type="application/pdf" data="http://localhost:8080/icopangkasa2/public/documents/CCA-2018-0062396_850418016087.pdf" id="pdf_content">
                                <p>Insert your error message here, if the PDF cannot be displayed.</p>
                            </object>
                    </div>
                </div>-->
            </div>
        </div>
    </div>
</div>
@endsection

@push('scripts')



<script type="text/javascript">
    $( "#agent_type" ).change(function() {
        var agent_type = $('#agent_type').val();
            $.ajax({
                url: "<?php  print url('/'); ?>/agent_type/"+agent_type,
                dataType: 'json',
                data: { 
                },
                beforeSend: function() { 
                    $("#agent_no").prop('agent_no', true); // disable button
                },
                success: function (data, status) {
                    $('#agent_no').html('<option></option>').trigger('change');
                    jQuery.each(data, function (k) {
                    $("#agent_no").append($('<option>',
                    {
                        value : data[k].id,
                        text : data[k].agent_number_code,
                    }));
                });
                $("#agent_no").prop('disabled', false);
            }
        });
    });
</script>

<script type="text/javascript">
    $( "#card_branch_state" ).change(function() {
        var card_branch_state = $('#card_branch_state').val();
            $.ajax({
                url: "<?php  print url('/'); ?>/card_branch_state/"+card_branch_state,
                dataType: 'json',
                data: { 
                },
                beforeSend: function() { 
                    $("#card_branch").prop('card_branch', true); // disable button
                },
                success: function (data, status) {
                    $('#card_branch').html('<option></option>').trigger('change');
                    jQuery.each(data, function (k) {
                    $("#card_branch").append($('<option>',
                    {
                        value : data[k].id,
                        text : data[k].card_branch_desc,
                    }));
                });
                $("#card_branch").prop('disabled', false);
            }
        });
    });
</script>

<script type="text/javascript">
    $( "#product_type" ).change(function() {
        var product_type = $('#product_type').val();
            $.ajax({
                url: "<?php  print url('/'); ?>/product_type/"+product_type,
                dataType: 'json',
                data: { 
                },
                beforeSend: function() { 
                    $("#program_code").prop('program_code', true); // disable button
                },
                success: function (data, status) {
                    $('#program_code').html('<option></option>').trigger('change');
                    jQuery.each(data, function (k) {
                    $("#program_code").append($('<option>',
                    {
                        value : data[k].id,
                        text : data[k].programme_code_desc,
                    }));
                });
                $("#program_code").prop('disabled', false);
            }
        });
    });
</script>

<script type="text/javascript">
    $(function(){
    var dtToday = new Date();
     var day = dtToday.getDate();
    var month = dtToday.getMonth() + 1;
   
    var year = dtToday.getFullYear();
     if(day < 10)
        day = '0' + day.toString();
    
    if(month < 10)
        month = '0' + month.toString();
   
    var maxDate = day + '-' + month + '-' + year;
   
    $('#date_applied').attr('max', maxDate);
});
</script>

<script type="text/javascript">
/**
 * File: js/showhide.js
 * Author: design1online.com, LLC
 * Purpose: toggle the visibility of fields depending on the value of another field
 **/
$(document).ready(function () {
    toggleFields(); //call this first so we start out with the correct visibility depending on the selected form values
    //this will call our toggleFields function every time the selection value of our underAge field changes
    $("#type_insurance, #purpose_insurance").change(function () {
        toggleFields();
    });

});
//this toggles the visibility of our parent permission fields depending on the current selected value of the underAge field
function toggleFields() {
  if ($("#card_collection").val() == 'B')
  {
    $("#card_branch_state").show();
    $("#card_branch").show();

  }
  else if ($("#card_collection").val() == 'R')
  {
     $("#card_branch_state").hide();
    $("#card_branch").hide();


  }
  else if ($("#type_insurance").val() == 'N')
  {
     $("#card_branch_state").hide();
    $("#card_branch").hide();

  }
}
</script>
<script type="text/javascript">
 $(function(){
    $("sc_channel").change(function(){
        if ( $(this).val() != 0 ) {
            $('#sc_branch').prop('disabled', true);
           
        }else {
            $(' #sc_branch').prop('disabled', false);
        }
    });
});
</script>

<script type="text/javascript">

$( "#postcode_" ).change(function() {
    var postcode = $('#postcode').val();
     
  $.ajax({
                url: "<?php  print url('/'); ?>/postcode/"+postcode,
                dataType: 'json',
                data: {
                   
                },
                success: function (data, status) {

                    jQuery.each(data, function (k) {

                        $("#city").val(data[k].post_office );
                        $("#state").val(data[k].state.state_name );
                        $("#country").val("Malaysia");
                    });

                }
            });

   
});
</script>
<script type="text/javascript">
    $(document).ready(function () {
     
        $("select").not($(":button")).keypress(function (evt) {
            if (evt.keyCode == 13) {
                iname = $(this).val();
                if (iname !== 'Submit') {
                    var fields = $(this).parents('form:eq(0),body').find('button, input, textarea, select');
                    var index = fields.index(this);
                    if (index > -1 && (index + 1) < fields.length) {
                        fields.eq(index + 1).focus();
                    }
                    return false;
                }
            }
        });
    });
</script>

@endpush