@extends('layouts.main')

@section('content')
  <section class="probootstrap-cover overflow-hidden relative"  style="background-image: url('{{ asset('assets/images/kl.jpg') }}');" data-stellar-background-ratio="0.5"  id="section-home">
    <div class="overlay"></div>
    <div class="container-fluid">
      <div class="row edit">
         
        <div class="col-md-6">
         
            <iframe id="optomaFeed" src="{{url('/')}}/data_entry/step_1/{{$temp_entry->account}}" scrolling="no" frameborder="0" height="680px"  width="100%" style="background-color: pink"  class="probootstrap-form border border-danger"></iframe>
        </div>   

        <div class="col-md-6">
          
            <iframe src="{{asset('documents/CCA-2018-0062396_850418016086.pdf')}}" style="margin-left: -10px" height="680px"  width="100%" class="probootstrap-form border border-danger"></iframe>

        </div>
      </div>
    </div>
 
     
        

     
  </section><!-- END section -->

<!-- Check -->
<script>
  function check() {
      document.getElementById("myCheck").checked = true;
  }           
</script>
<!-- End of Check -->  



@endsection
