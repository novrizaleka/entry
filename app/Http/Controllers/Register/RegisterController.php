<?php
namespace App\Http\Controllers\Register;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use View;
use File;
use App\Model\Entry\TemporaryEntries;
use App\Model\Register\CCRefnumbers;
use App\Model\Register\CCRegisters;
use Auth;
use App\User;
class RegisterController extends Controller
{
     public function inde2x()
    {
        return view('public.entry.register_entry');
    }

    public function index()
    { 
        $user = Auth::user();
    	$files = File::allFiles('documents'); 
    	$cc_reg   = CCRegisters::orderBy('created_at','DESC')->get();
    	return view('public.register.register',compact('files','cc_reg','user'));
    }

    public function image(Request $request) 
    {
        $account = $request->input('account');
        $file = $request->input('file');
        $link = $request->input('link');
        $temporary  = new TemporaryEntries;
        $temporary->account     = $link;
        $temporary->file     = $file;
        $temporary->save();
        
        return redirect('/register')->with('message', 'success!!');
    }

    public function image_view($id) 
    {
    	$link      = $id;
        $cc_reg   = CCRegisters::latest('created_at')->where('account_id_no',$id)->limit('1')->first();
    	return view('public.register.register_image',compact('link','cc_reg'));
    }

     public function done_register_image(Request $request) 
    {
    	$user = Auth::user();

        $refno 		= $request->input('reference_number');
        $account 	= $request->input('account_no');
        $id_no 		= $request->input('id_no');
        $link       = $request->input('link');
        $cust_name 	= $request->input('customer_name');
        $today 		= date('Y-m-d H:i:s');
        $user_id 	= $user->user_id;

        $validate   = CCRefnumbers::latest('created_at')->where('refno', $refno)->count();
        
        if ($validate == 0) 
            {
                $cc_ref  					= new CCRefnumbers;
                $cc_ref->refno     			= $refno;
                $cc_ref->date_registration  = $today;
                $cc_ref->user_id     		= $user_id;
                $cc_ref->save();
            }
        
        $cc_reg  					= new CCRegisters;
        $cc_reg->REG_RefNo     		= $refno;
        $cc_reg->account_id_no      = $link;
        $cc_reg->AccountID     		= $account;
        $cc_reg->IDNo     			= $id_no;
        $cc_reg->CustName     		= $cust_name;
        $cc_reg->DateReceived  		= $today;
        $cc_reg->DateRegister  		= $today;
        $cc_reg->UserReg     		= $user_id;
        $cc_reg->save();

        return redirect('/register')->with('message', 'success!!');
    }

}
