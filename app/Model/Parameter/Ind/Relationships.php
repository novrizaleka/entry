<?php

namespace App\Model\Parameter\Ind;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Relationships extends Model
{
    protected $table = 'ind_relationships';
    protected $fillable = [
       'relationship_code','relationship_desc','is_active'
    ];
    use SoftDeletes;

    protected $guarded = ["id"]; 
    protected $dates = ['deleted_at'];
    public $timestamps = true;
}

