<?php

namespace App\Model\Parameter\Ind;

use Illuminate\Database\Eloquent\Model;

class Occupations extends Model
{
    protected $table = 'ind_occupations';
}
