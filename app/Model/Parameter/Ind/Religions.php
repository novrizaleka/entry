<?php

namespace App\Model\Parameter\Ind;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Religions extends Model
{
    protected $table = 'ind_religions';
    protected $fillable = [
       'religion_code','religion_desc','is_active'
    ];
    use SoftDeletes;

    protected $guarded = ["id"]; 
    protected $dates = ['deleted_at'];
    public $timestamps = true;
}
