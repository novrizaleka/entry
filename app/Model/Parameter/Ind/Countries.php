<?php

namespace App\Model\Parameter\Ind;

use Illuminate\Database\Eloquent\Model;

class Countries extends Model
{
    protected $table = 'ind_countries';
}
