<?php

namespace App\Model\Parameter\Ind;

use Illuminate\Database\Eloquent\Model;

class Educations extends Model
{
    protected $table = 'ind_educations';
}
