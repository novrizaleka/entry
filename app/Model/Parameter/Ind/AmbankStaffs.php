<?php

namespace App\Model\Parameter\Ind;

use Illuminate\Database\Eloquent\Model;

class AmbankStaffs extends Model
{
    protected $table = 'ind_ambank_staffs';
}
