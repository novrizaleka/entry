<?php

namespace App\Model\Parameter\Ind;

use Illuminate\Database\Eloquent\Model;

class Ethnics extends Model
{
    protected $table = 'ind_ethnics';
}
