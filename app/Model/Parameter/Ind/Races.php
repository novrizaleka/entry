<?php

namespace App\Model\Parameter\Ind;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Races extends Model
{
    protected $table = 'ind_races';
    protected $fillable = [
       'race_code','race_desc','is_active'
    ];
    use SoftDeletes;

    protected $guarded = ["id"]; 
    protected $dates = ['deleted_at'];
    public $timestamps = true;
}
