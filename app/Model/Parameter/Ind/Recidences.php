<?php

namespace App\Model\Parameter\Ind;

use Illuminate\Database\Eloquent\Model;

class Recidences extends Model
{
    protected $table = 'ind_residences';
    protected $fillable = [
       'residence_code','residence_desc','is_active'
    ];

    protected $guarded = ["id"]; 
    protected $dates = ['deleted_at'];
    public $timestamps = true;
}
