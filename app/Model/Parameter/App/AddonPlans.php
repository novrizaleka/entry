<?php

namespace App\Model\Parameter\App;

use Illuminate\Database\Eloquent\Model;

class AddonPlans extends Model
{
   protected $table = 'app_addon_plans';
}
