<?php

namespace App\Model\Parameter\App;

use Illuminate\Database\Eloquent\Model;

class AddonPackages extends Model
{
    protected $table = 'app_addon_packages';
}
