<?php

namespace App\Model\Parameter\App;

use Illuminate\Database\Eloquent\Model;

class ProgrammeCodes extends Model
{
   protected $table = 'app_programme_codes';
}
