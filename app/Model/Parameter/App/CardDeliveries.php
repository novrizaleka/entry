<?php

namespace App\Model\Parameter\App;

use Illuminate\Database\Eloquent\Model;

class CardDeliveries extends Model
{
    protected $table = 'app_card_deliveries';
}
