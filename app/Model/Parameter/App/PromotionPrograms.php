<?php

namespace App\Model\Parameter\App;

use Illuminate\Database\Eloquent\Model;

class PromotionPrograms extends Model
{
   protected $table = 'app_promotion_programs';
}
