<?php

namespace App\Model\Parameter\App;

use Illuminate\Database\Eloquent\Model;

class BusinessOwners extends Model
{
   protected $table = 'app_business_owners';
}
