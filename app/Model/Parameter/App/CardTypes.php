<?php

namespace App\Model\Parameter\App;

use Illuminate\Database\Eloquent\Model;

class CardTypes extends Model
{
    protected $table = 'app_card_types';
}
