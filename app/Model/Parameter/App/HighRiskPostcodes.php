<?php

namespace App\Model\Parameter\App;

use Illuminate\Database\Eloquent\Model;

class HighRiskPostcodes extends Model
{
    protected $table = 'app_highrisk_postcodes';
}
