<?php

namespace App\Model\Register;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class CCRegisters extends Model
{
    protected $table = 'register_cc_registers';
    protected $fillable = [
       'REG_RefNo','AccountID','IDNo','CustName','Product','SourceCode','AgentID','Priority','UserReg','UserEnt','UserVer','DateReceived','DateRegister','DateEntrySt','DateEntryEnd','DateMerged','DateVerifySt','DateVerifyEnd','DateMerged2','DOMerged2','SaveToSQLSvr2','IDNo2','CustName2','RejectCode','account_id_no'
    ];
    use SoftDeletes;

    protected $guarded = ["id"]; 
    protected $dates = ['deleted_at'];
    public $timestamps = true;
}
