<?php

use Illuminate\Database\Seeder;
use App\Model\Parameter\Ind\Relationships;

class RelationshipsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
		Relationships::create([
            'relationship_code' => '1',
            'relationship_desc' => 'SPOUSE',
            'is_active' => '1'
        ]);

        Relationships::create([
            'relationship_code' => '2',
            'relationship_desc' => 'OFFSPRING',
            'is_active' => '1'
        ]);

        Relationships::create([
            'relationship_code' => '3',
            'relationship_desc' => 'PARENTS',
            'is_active' => '1'
        ]);

        Relationships::create([
            'relationship_code' => '4',
            'relationship_desc' => 'SIBLING',
            'is_active' => '1'
        ]);

        Relationships::create([
            'relationship_code' => '5',
            'relationship_desc' => 'FIANCE / FIANCEE',
            'is_active' => '1'
        ]);

        Relationships::create([
            'relationship_code' => '6',
            'relationship_desc' => 'OTHER NON-IMMEDIATE FAMILY RELATIONS',
            'is_active' => '1'
        ]);

        Relationships::create([
            'relationship_code' => '8',
            'relationship_desc' => 'FRIENDS',
            'is_active' => '1'
        ]);

        Relationships::create([
            'relationship_code' => 'A',
            'relationship_desc' => 'HOLDING COMPANY',
            'is_active' => '1'
        ]);

        Relationships::create([
            'relationship_code' => 'B',
            'relationship_desc' => 'SUBSIDIARY',
            'is_active' => '1'
        ]);

        Relationships::create([
            'relationship_code' => 'C',
            'relationship_desc' => 'SHAREHOLDER',
            'is_active' => '1'
        ]);

        Relationships::create([
            'relationship_code' => 'D',
            'relationship_desc' => 'DIRECTOR',
            'is_active' => '1'
        ]);

        Relationships::create([
            'relationship_code' => 'E',
            'relationship_desc' => 'PARTNERSHIP',
            'is_active' => '1'
        ]);

        Relationships::create([
            'relationship_code' => 'F',
            'relationship_desc' => 'SOLE PROPRIETOR',
            'is_active' => '1'
        ]);

        Relationships::create([
            'relationship_code' => 'W',
            'relationship_desc' => ' SOLE PROPRIETOR',
            'is_active' => '1'
        ]);

        Relationships::create([
            'relationship_code' => 'X',
            'relationship_desc' => 'COMPANY PARTNER',
            'is_active' => '1'
        ]);

        Relationships::create([
            'relationship_code' => 'Y',
            'relationship_desc' => ' COMPANY OWNER',
            'is_active' => '1'
        ]);

        Relationships::create([
            'relationship_code' => 'Z',
            'relationship_desc' => ' COMPANY DIRECTOR',
            'is_active' => '1'
        ]);
    }
}
