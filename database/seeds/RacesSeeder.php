<?php

use Illuminate\Database\Seeder;
use App\Model\Parameter\Ind\Races;

class RacesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
		Races::create([
            'id' => '1',
            'race_code' => '1',
            'race_desc' => 'Malay',
            'is_active' => '1'
        ]);

        Races::create([
            'id' => '2',
            'race_code' => '2',
            'race_desc' => 'Chinese',
            'is_active' => '1'
        ]);

        Races::create([
            'id' => '3',
            'race_code' => '3',
            'race_desc' => 'Indian',
            'is_active' => '1'
        ]);

        Races::create([
            'id' => '4',
            'race_code' => '4',
            'race_desc' => 'Bumiputra Sabah',
            'is_active' => '1'
        ]);

        Races::create([
            'id' => '5',
            'race_code' => '5',
            'race_desc' => 'Bumiputra Sarawak',
            'is_active' => '1'
        ]);

        Races::create([
            'id' => '6',
            'race_code' => '6',
            'race_desc' => 'Pribumi (Native)',
            'is_active' => '1'
        ]);

        Races::create([
            'id' => '9',
            'race_code' => '9',
            'race_desc' => 'Others',
            'is_active' => '1'
        ]);
    }
}
