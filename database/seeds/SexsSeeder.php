<?php

use Illuminate\Database\Seeder;
use App\Model\Parameter\Ind\Sexs;

class SexsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
		Sexs::create([
            'sexs_code' => '1',
            'sexs_desc' => 'Male',
            'is_active' => '1'
        ]);

        Sexs::create([
            'sexs_code' => '2',
            'sexs_desc' => 'Female',
            'is_active' => '1'
        ]);

        Sexs::create([
            'sexs_code' => '3',
            'sexs_desc' => 'Impersonal',
            'is_active' => '1'
        ]);
    }
}
