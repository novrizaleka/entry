<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateADDONsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if(!Schema::hasTable('addons'))
            {
            Schema::create('addons', function (Blueprint $table) {
                $table->increments('id');
                $table->String('AddOn_Unique_ID',20)->nullable();
                $table->String('Application_Unique_ID',20)->nullable();
                $table->String('Credit_Card_Unique_ID',20)->nullable();
                $table->String('Add_On',20)->nullable(); //plan s3
                $table->String('Sub_Add_On',20)->nullable(); //package s3
                $table->timestamps();
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('addons');
    }
}
