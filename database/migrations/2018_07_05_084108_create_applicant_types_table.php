<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateApplicantTypesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
         if(!Schema::hasTable('ind_applicant_types'))
        {
            Schema::create('ind_applicant_types', function (Blueprint $table) {
                $table->increments('id');
                $table->String('applicant_type_code',2)->nullable();
                $table->String('applicant_type_desc',50)->nullable();
                $table->boolean('is_active')->default(1)->nullable();
                $table->softDeletes();
                $table->timestamps();
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('ind_applicant_types');
    }
}
