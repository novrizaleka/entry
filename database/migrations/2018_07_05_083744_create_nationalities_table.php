<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateNationalitiesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
         if(!Schema::hasTable('ind_nationalities'))
        {
            Schema::create('ind_nationalities', function (Blueprint $table) {
                $table->increments('id');
                $table->String('nationality_code',2)->nullable();
                $table->String('nationality_code_desc',100)->nullable();
                $table->boolean('is_active')->default(1)->nullable();
                $table->softDeletes();
                $table->timestamps();
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('ind_nationalities');
    }
}
