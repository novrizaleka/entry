<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateFeeCodesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if(!Schema::hasTable('app_fee_codes'))
        {
            Schema::create('app_fee_codes', function (Blueprint $table) {
                $table->increments('id');
                $table->String('fee_code_type',10)->nullable();
                $table->String('program_code',20)->nullable();
                $table->String('card_type',20)->nullable();
                $table->integer('card_type_code')->nullable();
                $table->integer('fee_code')->nullable();
                $table->String('fee_code_desc',100)->nullable();
                $table->boolean('is_active')->default(1)->nullable();
                $table->softDeletes();
                $table->timestamps();
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('app_fee_codes');
    }
}
