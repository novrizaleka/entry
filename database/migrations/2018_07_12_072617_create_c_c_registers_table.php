<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCCRegistersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */

    public function up()
    {
        if(!Schema::hasTable('register_cc_registers'))
        {
           Schema::create('register_cc_registers', function (Blueprint $table) {
                $table->increments('id');
                 $table->String('account_id_no',100)->nullable();
                $table->String('REG_RefNo',100)->nullable();
                $table->String('AccountID',100)->nullable();
                $table->String('IDNo',12)->nullable();
                $table->String('CustName',50)->nullable();
                $table->String('Product',35)->nullable();
                $table->String('SourceCode',6)->nullable();
                $table->String('AgentID',12)->nullable();
                $table->boolean('Priority')->nullable();
                $table->String('UserReg',4)->nullable();
                $table->String('UserEnt',4)->nullable();
                $table->String('UserVer',4)->nullable();
                $table->datetime('DateReceived')->nullable();
                $table->datetime('DateRegister')->nullable();
                $table->datetime('DateEntrySt')->nullable();
                $table->datetime('DateEntryEnd')->nullable();
                $table->datetime('DateMerged')->nullable();
                $table->datetime('DateVerifySt')->nullable();
                $table->datetime('DateVerifyEnd')->nullable();
                $table->datetime('DateMerged2')->nullable();
                $table->boolean('DOMerged2')->nullable();
                $table->boolean('SaveToSQLSvr2')->nullable();
                $table->String('IDNo2',12)->nullable();
                $table->String('CustName2',50)->nullable();
                $table->String('RejectCode',2)->nullable();
                $table->softDeletes();
                $table->timestamps();
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('register_cc_registers');
    }
}
