<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRELsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if(!Schema::hasTable('rels'))
            {
            Schema::create('rels', function (Blueprint $table) {
                $table->increments('id');
                $table->String('Customer_Related_Unique_ID',25)->nullable();
                $table->String('Customer_Unique_ID',25)->nullable();
                $table->String('Application_Unique_ID',25)->nullable();
                $table->String('Relationship_to_Principal',20)->nullable();
                $table->String('Customer_Name',150)->nullable();
                $table->String('ID_Type',20)->nullable(); //ID type S9
                $table->String('ID_No',20)->nullable(); //ID No S9
                $table->String('Alternate_Id_No',20)->nullable();  //Alterna ID No
                //Alternate ID Type (Tisak ada)
                $table->String('Address_1',100)->nullable();   //Residence Address s11
                $table->String('Address_2',100)->nullable(); //Residence Address s11
                $table->String('Address_3',100)->nullable(); //Residence Address s11
                $table->String('Town',20)->nullable();
                $table->String('State',20)->nullable(); //State s11
                $table->String('Country',20)->nullable(); //State s11
                $table->String('Postcode',10)->nullable(); //Postcode s11
                $table->integer('Telephone_No_Area_Code')->nullable(); //Home telp no area s9
                $table->integer('Telephone_No')->nullable(); //Home telp no s9
                $table->integer('Mobile_Area_Code')->nullable();
                $table->integer('Mobile')->nullable();
                $table->String('Employment_Type',20)->nullable(); //employment type s12
                $table->String('Occupation',20)->nullable(); //Occupation s9
                $table->String('Other_Occupation',100)->nullable(); //other Occupation s9
                $table->String('Employer_Name',100)->nullable(); //Employment Name s12
                $table->String('Office_Address_1',100)->nullable(); //Employer Address s12
                $table->String('Office_Address_2',100)->nullable(); //Employer Address s12
                $table->String('Office_Address_3',100)->nullable(); //Employer Address s12
                $table->String('Town',20)->nullable(); //city S12
                $table->String('State',20)->nullable(); //state s12
                $table->String('Country',20)->nullable(); //Country s12
                $table->integer('Postcode')->nullable(); //postcode s12
                $table->integer('Office_Telephone_No_Area_Code')->nullable(); //Office_Telephone_No s9
                $table->integer('Office_Telephone_No')->nullable(); //Office_Telephone_No s9
                $table->integer('Gross_Monthly_Income')->nullable();
                $table->integer('Net_Monthly_Income')->nullable();
                $table->timestamps();
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('rels');
    }
}
