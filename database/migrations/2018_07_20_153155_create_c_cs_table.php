<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCCsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        
        if(!Schema::hasTable('ccs'))
            {
            Schema::create('ccs', function (Blueprint $table) {
                $table->increments('id');
                $table->String('Credit_Card_Unique_ID',25)->nullable(); 
                $table->String('Application_Unique_ID',25)->nullable();
                $table->String('Customer_Unique_ID',25)->nullable();
                $table->String('Applicant_Type_',20)->nullable();
                $table->integer('Principal_Card_No')->nullable();
                $table->String('Principal_Card_Type',20)->nullable();
                $table->String('Product_Type',20)->nullable(); //Product Type s2
                $table->String('Source_Code_Channel',20)->nullable(); //Sc channel Branch s1
                $table->String('Source_Code_Branch',20)->nullable(); //Sc channel Branch s1
                $table->String('Agent_Code',20)->nullable(); //Agent code s1
                $table->String('Agent_No',20)->nullable(); //Agent code s1
                $table->String('Plastic_Data_E4',20)->nullable(); 
                $table->String('Card_Type',20)->nullable(); //Card Type s2
                $table->String('Program_Code',20)->nullable(); // Prg Code S2
                $table->String('Gift_Code',20)->nullable(); //Gift s2
                $table->String('Fee_Code',20)->nullable(); 
                $table->String('fth_Line_Embossing',19)->nullable();
                $table->char('Zing_Indicator',1)->nullable(); //Zing Ind s2 
                $table->timestamps();
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('ccs');
    }
}
