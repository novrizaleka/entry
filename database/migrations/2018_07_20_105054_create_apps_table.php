<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAppsTable extends Migration
{
    /**
     * Run the migrations.
     *




     * @return void
     */
    public function up()
    {
        if(!Schema::hasTable('apps'))
            {
            Schema::create('apps', function (Blueprint $table) {
                $table->increments('id');
                $table->String('Outsourcer_Batch_No',25)->nullabe();
                $table->String('Application_Unique_Id',25)->nullabe();
                $table->tinyInteger('Total_CIF')->nullabe()->unsigned();
                $table->tinyInteger('Total_CC')->nullabe()->unsigned();
                $table->tinyInteger('Total_REL')->nullabe()->unsigned();
                $table->String('Reference_No',22)->nullabe();
                $table->String('Pre_Screen_Application',20)->nullabe();
                $table->String('Product_Bundling_Ref_No',22)->nullabe();
                $table->String('Application_Type',20)->nullabe();
                $table->String('Originator_Branch',20)->nullabe();
                $table->String('Processing_Center',20)->nullabe();
                $table->String('Application_Category',20)->nullabe();
                $table->Date('Date_Applied')->nullabe();
                $table->Date('Date_Received')->nullabe();
                $table->String('Application_Source',20)->nullabe();
                $table->String('Application_Purpose',20)->nullabe();
                $table->String('Business_Unit',20)->nullabe();
                $table->String('Company_ID',7)->nullabe();
                $table->String('Urgent_Application',1)->nullabe();
                $table->String('Urgent_Remark',250)->nullabe();
                $table->String('Promotion_Program',20)->nullabe();
                $table->String('Review_code',20)->nullabe();
                $table->String('Introducers_Name',50)->nullabe();
                $table->String('Telephone_No_Area_Code',3)->nullabe();
                $table->integer('Telephone_No')->nullabe()->unsigned();
                $table->integer('Telephone_Ext')->nullabe()->unsigned();
                $table->integer('Credit_Card_No')->nullabe()->unsigned();
                $table->integer('ID_No')->nullabe()->unsigned();
                $table->String('Handphone_No_Area_Code',3)->nullabe();
                $table->integer('Handphone_No')->nullabe()->unsigned();
                $table->String('Current_Account_Savings_Account',20)->nullabe();
                

                /* S Profile Indicator & Special Interest */
                $table->String('VIP',1)->nullabe(); //VIP
                $table->String('Staff',1)->nullabe(); //Staff
                $table->String('Info_Incomplete_Info',1)->nullable(); //info
                $table->String('Serap',1)->nullabe(); //Serap
                $table->String('SE_Staff_S',1)->nullabe();  //SE_Staff_S
                
                $table->String('Incomplete_Document',250)->nullable();
                $table->String('FR',1)->nullable(); //fr
                $table->String('BJR',1)->nullable(); //
                $table->String('Res_Ind',1)->nullable();
                $table->String('Staff_Get_Member',1)->nullable();
                $table->String('Guaranteed',1)->nullable();
                $table->String('PIN_Generation',1)->nullable();
                $table->String('Foreign_Individual',1)->nullable();
                $table->String('AM_Protector',1)->nullable();
                $table->String('FD_Pledge_Indicator',1)->nullable();
                $table->String('Direct_Mailing_Indicator',1)->nullable();
                $table->tinyInteger('Total_Add_On')->nullable()->unsigned();
                $table->String('Special_Interest',20)->nullable();
                $table->String('Opt_out_Excess_Limit_Fees',1)->nullable();
                $table->timestamps();
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('apps');
    }
}
