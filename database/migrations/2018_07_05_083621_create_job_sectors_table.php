<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateJobSectorsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if(!Schema::hasTable('ind_job_sectors'))
        {
            Schema::create('ind_job_sectors', function (Blueprint $table) {
                $table->increments('id');
                $table->String('job_sector_type',2)->nullable();
                $table->String('job_sector_code',2)->nullable();
                $table->String('job_sector_desc',100)->nullable();
                $table->boolean('is_active')->default(1)->nullable();
                $table->softDeletes();
                $table->timestamps();
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('ind_job_sectors');
    }
}
