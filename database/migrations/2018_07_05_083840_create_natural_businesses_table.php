<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateNaturalBusinessesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
         if(!Schema::hasTable('ind_nature_of_businesses'))
        {
            Schema::create('ind_nature_of_businesses', function (Blueprint $table) {
                $table->increments('id');
                $table->String('nature_of_business_code',5)->nullable();
                $table->String('nature_of_business_desc',100)->nullable();
                $table->boolean('is_active')->default(1)->nullable();
                $table->softDeletes();
                $table->timestamps();
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('ind_nature_of_businesses');
    }
}
