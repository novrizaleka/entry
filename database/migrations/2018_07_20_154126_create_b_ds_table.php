<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBDsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if(!Schema::hasTable('bds'))
            {
            Schema::create('bds', function (Blueprint $table) {
                $table->String('Application_Unique_ID',25)->nullable();
                $table->String('Bundle_Unique_ID',25)->nullable();
                $table->String('Marketer_Team',20)->nullable();
                $table->String('Marketer',20)->nullable();
                $table->String('AmFast_PBAgent',20)->nullable();
                $table->String('Purpose_of_Facility',20)->nullable();
                $table->String('Housing_Market',20)->nullable();
                $table->String('Facility_Unique_ID',20)->nullable();
                $table->String('Application_Unique_ID_fasility_de',50)->nullable();
                $table->String('Facility_Category',20)->nullable();
                $table->String('Facility',20)->nullable();
                $table->String('Currency',20)->nullable();
                $table->integer('Facility_Amount')->nullable();
                $table->integer('Other_Fees_Amount')->nullable();
                $table->integer('Facility_Tenure_For_TCL')->nullable();
                $table->char('MRTA_Financed',1)->nullable();
                $table->String('MRTA_Mode',20)->nullable();
                $table->integer('Margin_of_MRTA_Financed')->nullable();
                $table->integer('MRTA_Financed_Amount')->nullable();
                $table->String('Rest_Frequency',20)->nullable();
                $table->integer('Total_Facility_Amount')->nullable();
                $table->integer('Share_Percentage')->nullable();
                $table->String('Purpose_Code',20)->nullable();
                $table->String('BNM_Code',20)->nullable();
                $table->String('Facility_Account_No',27)->nullable();
                $table->String('Facility_Tagging',20)->nullable();
                $table->String('Remark',400)->nullable();
                $table->integer('BLR')->nullable();
                $table->integer('IFR')->nullable();
                $table->String('Facility_Package',20)->nullable();
                $table->String('Pricing_Option',20)->nullable();
                $table->String('LO_Description',20)->nullable();
                $table->integer('Facility_Tenure')->nullable();
                $table->String('Rate_Type',20)->nullable();
                $table->String('Sign',20)->nullable();
                $table->integer('Margin')->nullable();
                $table->integer('Prescribe_Rate')->nullable();
                $table->String('Desc_1',20)->nullable();
                $table->String('Desc_2',20)->nullable();
                $table->integer('Selling_Price')->nullable();
                $table->integer('Unearned_Income')->nullable();
                $table->timestamps();
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('b_ds');
    }
}
