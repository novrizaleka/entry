<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAmFastsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
          if(!Schema::hasTable('bun_am_fasts'))
        {
            Schema::create('bun_am_fasts', function (Blueprint $table) {
                    $table->increments('id');
                    $table->String('am_fasts_code',20)->nullable();
                    $table->String('am_fasts_desc',100)->nullable();
                    $table->boolean('is_active')->default(1)->nullable();
                    $table->softDeletes();
                    $table->timestamps();
                });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('bun_am_fasts');
    }
}
