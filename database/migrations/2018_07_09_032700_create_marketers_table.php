<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMarketersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
         if(!Schema::hasTable('bun_marketers'))
        {
            Schema::create('bun_marketers', function (Blueprint $table) {
                    $table->increments('id');
                    $table->String('marketer_number',20)->nullable();
                    $table->String('marketer_code',20)->nullable();
                    $table->String('marketer_name',100)->nullable();
                    $table->boolean('is_active')->default(1)->nullable();
                    $table->softDeletes();
                    $table->timestamps();
                });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('bun_marketers');
    }
}
