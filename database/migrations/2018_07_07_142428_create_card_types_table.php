<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCardTypesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if(!Schema::hasTable('app_card_types'))
        {
            Schema::create('app_card_types', function (Blueprint $table) {
                $table->increments('id');
                $table->String('card',2)->nullable();
                $table->integer('card_type')->nullable();
                $table->integer('estatement_code_conv')->nullable();
                $table->integer('estatement_code_islamic')->nullable();
                $table->String('card_type_desc',100)->nullable();
                $table->boolean('is_active')->default(1)->nullable();
                $table->softDeletes();
                $table->timestamps();
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('app_card_types');
    }
}
