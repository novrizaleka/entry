<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTemporaryIndCifInformationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
       if(!Schema::hasTable('main_temporary_ind_cif_informations'))
        {
            Schema::create('main_temporary_ind_cif_informations', function (Blueprint $table) {

                    $table->increments('id');
                    $table->String('user_id',20)->nullable();
                    $table->String('temp_tab4_id_type',20)->nullable();
                    $table->String('temp_tab4_id_no',100)->nullable();
                    $table->String('temp_tab4_alternate_id_type',100)->nullable();
                    $table->String('temp_tab4_alternate_id_no',100)->nullable();
                    $table->String('temp_tab4_customer_name',100)->nullable();
                    $table->String('temp_tab4_salutation',100)->nullable();
                    $table->String('temp_tab4_name_on_card',100)->nullable();
                    $table->String('temp_tab4_sex',100)->nullable();
                    $table->String('temp_tab4_race',100)->nullable();
                    $table->String('temp_tab4_religion',100)->nullable();
                    $table->String('temp_tab4_ethnic',100)->nullable();
                    $table->String('temp_tab4_nationality',100)->nullable();
                    $table->String('temp_tab4_citizenshipbg',100)->nullable();
                    $table->String('temp_tab4_date_applied',100)->nullable();
                    $table->String('temp_tab4_aip_ref_no',100)->nullable();

                    $table->String('id_type',100)->nullable();
                    $table->String('id_no',100)->nullable();
                    $table->String('alternate_id_type',100)->nullable();

                    $table->String('alternate_id_no',100)->nullable();
                    $table->String('customer_name',100)->nullable();
                    $table->String('salutation',100)->nullable();
                    $table->boolean('is_active')->default(1)->nullable();
                    $table->softDeletes();
                    $table->timestamps();
                });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('main_temporary_ind_cif_informations');
    }
}
