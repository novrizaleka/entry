<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAgentNosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
          if(!Schema::hasTable('app_agent_numbers'))
        {
            Schema::create('app_agent_numbers', function (Blueprint $table) {
                $table->increments('id');
                $table->String('agent_number_type',2)->nullable();
                $table->String('agent_number_code',20)->nullable();
                $table->String('ambank_staff_desc',100)->nullable();
                $table->boolean('is_active')->default(1)->nullable();
                $table->softDeletes();
                $table->timestamps();
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('app_agent_numbers');
    }
}
