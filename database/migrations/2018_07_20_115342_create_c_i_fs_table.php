<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCIFsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     * 
     */
    


    public function up()
    {
        if(!Schema::hasTable('cifs'))
            {
            Schema::create('cifs', function (Blueprint $table) {
                $table->increments('id');
                $table->String('Customer_Unique_ID',25)->nullable();
                $table->String('Application_Unique_ID',20)->nullable();
                $table->String('Customer_Type',20)->nullable();
                $table->String('Salutation',20)->nullable(); //Salutation s4
                $table->String('Is_Bank_Staff',20)->nullable(); //AmBank Staff s4
                $table->String('Customer_Name_P_Supplementary_Name',150)->nullable(); //Customer Name s4
                $table->String('Alias_Name',50)->nullable();
                $table->char('VIP_Customer',1)->nullable(); //VIP_Customer
                $table->String('Citizen',20)->nullable(); //Citizenship s4
                $table->String('Customer_Title',20)->nullable(); //Customer Title
                $table->String('ID_Type',20)->nullable();//ID Type S4
                $table->String('ID_No',20)->nullable(); //ID No s4
                $table->String('Passport_Country_of_Issue',20)->nullable();
                $table->Date('Passport_Expiry_Date',20)->nullable();
                $table->String('Alternate_Id_type',20)->nullable(); //Alternate ID Type s4
                $table->String('Alternate_Id_No',20)->nullable(); //Alternate ID No s4
                $table->String('Income_Tax_No',20)->nullable();
                $table->String('Driving_License_No',10)->nullable();
                $table->Date('Date_of_Birth',20)->nullable(); //Date Of Birth s4
                $table->integer('Age',20)->nullable();
                $table->String('Race',20)->nullable(); //Race s4
                $table->String('Religion',20)->nullable(); //Religion s4
                $table->String('Nationality',20)->nullable(); //Nasionality s4
                $table->String('Gender',20)->nullable(); //Sex s4
                $table->String('Marital_Status',20)->nullable(); //Marital Status s4
                $table->String('Bumi_Status',20)->nullable(); //Ethnic s4
                $table->integer('No_of_Dependents',100)->nullable(); //No_of_Dependents s4
                $table->String('Mother_Maiden_Name',20)->nullable(); //Mothers Name
                $table->String('Education',21)->nullable(); //Education Level s4
                $table->String('Name_to_Appear_On_Card',50)->nullable(); //Name On Card s4
                $table->String('Email',50)->nullable(); //email s5
                $table->integer('Alternate_Mobile_No_Area_Code')->nullable();
                $table->integer('Alternate_Mobile_No')->nullable();
                $table->integer('Mobile_Area_Code')->nullable();
                $table->integer('Mobile')->nullable();
                $table->String('Card_Delivery_Address',20)->nullable();
                $table->String('Card_Collection_Method',20)->nullable();
                $table->String('Collect_at_Branch',20)->nullable();
                $table->String('Address_1_res',100)->nullable(); //Home Address 1 s5
                $table->String('Address_2_res',100)->nullable(); //Home Address 2 s5
                $table->String('Address_3_res',100)->nullable(); //Home Address 3 s5
                $table->String('City',20)->nullable(); //State s5
                $table->String('State',20)->nullable(); //City s5
                $table->String('Postcode',10)->nullable(); //Postcode s5
                $table->String('Country',20)->nullable(); //Country s5
                $table->String('Relationship_to_Principal',20)->nullable();
                $table->String('Applicant_Type_Individual',20)->nullable();
                $table->String('Residence_Type_res',20)->nullable(); //Residence Type s5
                $table->String('Address_1',100)->nullable();
                $table->String('Address_2',100)->nullable();
                $table->String('Address_3',100)->nullable();
                $table->String('Country',20)->nullable();
                $table->String('State',20)->nullable();
                $table->String('Postcode',10)->nullable();
                $table->integer('City',20)->nullable();
                $table->integer('Telephone_No_Area_Code')->nullable(); //Telephone_No_Area_Code s5
                $table->integer('Telephone_No')->nullable(); //Telephone_No s5
                $table->integer('Mobile_Phone_Service_Provider_No')->nullable(); //mobile s5
                $table->integer('Mobile_Phone_No')->nullable(); //mobile s5
                $table->integer('Fax_No_Area_Code')->nullable();
                $table->integer('Fax_No')->nullable();
                $table->integer('Total_Month_of_Staying')->nullable(); //Years of Stay -- kemungkinan s5
                $table->String('Mailing_Address_Indicator',20)->nullable();  //Mailing Address s5
                $table->String('Residence_Type',20)->nullable();
                $table->String('Address_1_per',100)->nullable(); //Home Address 1 s6
                $table->String('Address_2_per',100)->nullable(); //Home Address 2 s6
                $table->String('Address_3_per',100)->nullable();//Home Address 3 s6
                $table->String('Country_per',20)->nullable();  //Contry s6
                $table->String('State_per',20)->nullable(); //State s6
                $table->String('City_per',10)->nullable(); //City s6
                $table->integer('Postcode_per')->nullable(); //Postcode s6
                $table->integer('Telephone_No_Area_Code')->nullable();  //home telp no s6
                $table->integer('Telephone_No')->nullable();  //home telp no s6
                $table->integer('Total_Month_of_Staying')->nullable();  //Years of Stay -- kemungkinan s6
                $table->String('Employer_Name',100)->nullable(); //Employer Name s7
                $table->String('Occupation',20)->nullable(); // Occupation s8
                $table->String('Other_Occupation',100)->nullable();
                $table->String('Employment_Type',20)->nullable(); //Employment Type s7
                $table->String('Occupation_Description',100)->nullable(); //Occupation_Description s8
                $table->String('Job_Sector',20)->nullable(); //Job_Sector s8
                $table->String('Nature_Of_Business',20)->nullable(); //Nature_Of_Business s8
                $table->String('AMLA_Indicator',20)->nullable(); 
                $table->String('Risk_Level',20)->nullable();
                $table->String('Designation',20)->nullable(); //Designation s8
                $table->String('Job_Status_Employment_Status_S',20)->nullable();
                $table->Date('Date_Joined')->nullable(); //Date_Joined s8
                $table->integer('Total_Month_in_Company')->nullable();
                $table->String('Department',100)->nullable(); //Departement s7
                $table->String('Office_Address_1_emp',100)->nullable(); //Address 1 s7
                $table->String('Office_Address_2_emp',100)->nullable(); //Address 2 s7
                $table->String('Office_Address_3_emp',100)->nullable(); //Address 3 s7
                $table->String('Country_emp',20)->nullable(); //Country s7
                $table->String('State_emp',20)->nullable(); //State s7
                $table->String('City_emp',20)->nullable(); // city s7
                $table->String('Postcode_emp',10)->nullable(); //postcode s7
                $table->integer('Office_Telephone_No_Area_Code')->nullable(); //office telp no s7
                $table->integer('Office_Telephone_No')->nullable(); //office telp no s7
                $table->integer('Extension')->nullable(); 
                $table->integer('Office_Fax_No_Area_Code')->nullable(); //fax s7
                $table->integer('Office_Fax_No')->nullable(); //fax s7
                $table->integer('Mobile_No_Area_Code')->nullable(); 
                $table->integer('Mobile_No')->nullable();
                $table->String('Email_Office',50)->nullable(); //email s7
                $table->String('Previous_Employer',100)->nullable(); //Name of Company s8
                $table->String('Previous_Job_Sector',20)->nullable(); //Job Sector s8
                $table->String('Previous_Designation',20)->nullable(); //Previous_Designation s8
                $table->integer('Last_Drawn_Salary')->nullable();
                $table->Date('Previous_Joined_Date')->nullable(); //Date Joined s8
                $table->integer('Total_months_in_Previous_Company')->nullable(); //Years of Employment s8 kemungkinan
                $table->integer('Basic_Salary')->nullable();
                $table->integer('Allowance')->nullable();
                $table->integer('Other_Income')->nullable();
                $table->integer('Monthly_Total_Basic_Salary_Allowance_Other_Income')->nullable();
                $table->integer('Gross_Annual_Income')->nullable();
                $table->integer('Household_Expenses')->nullable();
                $table->integer('Other_Expenses')->nullable();
                $table->integer('Statutory_Deduction')->nullable();
                $table->integer('Total_Loan_Installment')->nullable();
                $table->integer('Monthly_Total_Expenses')->nullable();
                $table->integer('Monthly_Total_Net_Income')->nullable();  //Monthly Income s8
                $table->timestamps();
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('c_i_fs');
    }
}
