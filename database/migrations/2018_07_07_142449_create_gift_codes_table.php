<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateGiftCodesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if(!Schema::hasTable('app_gift_codes'))
        {
            Schema::create('app_gift_codes', function (Blueprint $table) {
                $table->increments('id');
                $table->String('card_type',10)->nullable();
                $table->String('program_code',20)->nullable();
                $table->String('gift_code',20)->nullable();
                $table->String('gift_code_desc',100)->nullable();
                $table->boolean('is_active')->default(1)->nullable();
                $table->softDeletes();
                $table->timestamps();
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('app_gift_codes');
    }
}
