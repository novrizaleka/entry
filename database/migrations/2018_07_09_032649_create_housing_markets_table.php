<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateHousingMarketsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
         if(!Schema::hasTable('bun_housing_markets'))
        {
            Schema::create('bun_housing_markets', function (Blueprint $table) {
                    $table->increments('id');
                    $table->integer('housing_market_code')->nullable();
                    $table->String('housing_market_desc',100)->nullable();
                    $table->boolean('is_active')->default(1)->nullable();
                    $table->softDeletes();
                    $table->timestamps();
                });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('bun_housing_markets');
    }
}
